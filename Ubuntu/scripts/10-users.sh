#!/bin/sh

#set -e
set -x


# Create an admin group,
# which will contain users that have passwordless sudo
groupadd admin

tee /tmp/vagrant << 'EOP'
# When sudo is invoked, keep the established ssh sock
# Defaults env_keep="SSH_AUTH_SOCK"

# Allow passwordless sudo for admin and vagrant
%admin ALL=(ALL) NOPASSWD:ALL
%sudo ALL=(ALL) NOPASSWD:ALL
vagrant ALL=(ALL) NOPASSWD: ALL
EOP
chmod 0440 /tmp/vagrant
mv /tmp/vagrant /etc/sudoers.d/
chown root:root /etc/sudoers.d/vagrant

# Create vagrant user
if ! id -u vagrant >/dev/null 2>&1; then
  echo '==> Creating Vagrant user'
  /usr/sbin/groupadd vagrant
  /usr/sbin/useradd vagrant -g vagrant -G admin  -G sudo --create-home
  # Set vagrant as password for usr vagrant
  echo vagrant:vagrant | sudo chpasswd
fi
