#!/bin/sh

#do not set -e, vbox or umount might break at the end, most probably umount
set -x

#install linux headers
aptitude install -y linux-headers-$(uname -r)

# install support for building kernel modules
aptitude install -y dkms
# install make, not sure of the reason its needed
aptitude install -y make

# Mount the disk image
cd /tmp

MOUNT_DIR=/tmp/mount-virtualbox-guest-additions
mkdir -p ${MOUNT_DIR}

VBOX_ISO_LOCATION=/home/vagrant/VBoxGuestAdditions.iso
mount -t iso9660 -o loop ${VBOX_ISO_LOCATION} ${MOUNT_DIR}

# Install the drivers
${MOUNT_DIR}/VBoxLinuxAdditions.run

# Cleanup
umount -vf ${MOUNT_DIR}
#rm -rf isomount /root/VBoxGuestAdditions.iso
rm -rf ${MOUNT_DIR}
